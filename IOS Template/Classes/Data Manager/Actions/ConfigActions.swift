//
//  PicklistActions.swift
//  Berluti
//
//  Created by Jeremy Martiano on 15/11/2017.
//  Copyright © 2017 elie buff. All rights reserved.
//

import Foundation
import ReSwift

struct ConfigSetItemsAction: Action {
    let items: [(label :String, value :String)]?
}

struct ConfigSelectItemsAction: Action {
    let selectedItems: [(label :String, value :String)]?
}

struct ConfigSetCurrentObject: Action{
    let currentObject:(object: String,field: String,label: UILabel,isLookup:Bool)?
}

struct IsLoadingConfigAction: Action{
    let isLoading: Bool?
}

