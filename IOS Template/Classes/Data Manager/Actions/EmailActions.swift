//
//  ProductActions.swift
//  IOS Template
//
//  Created by Shlomo Ariel on 31/05/2018.
//  Copyright © 2018 Balink. All rights reserved.
//

import ReSwift

struct EmailsIsLoadingAction: Action {
    let isLoading: Bool?
}

struct SetEmailListAction: Action {
    let emailItems: [EmailWrapper]?
}

struct SetSelectedEmailAction: Action{
    let selectedEmail: EmailWrapper?
}
