//
//  ProductActions.swift
//  IOS Template
//
//  Created by Shlomo Ariel on 31/05/2018.
//  Copyright © 2018 Balink. All rights reserved.
//

import ReSwift

struct ProductsIsLoadingAction: Action {
    let isLoading: Bool?
}

struct SetProductListAction: Action {
    let productItems: [ProductWrapper]?
}

struct SetSelectedProductsAction: Action{
    let selectedProducts: [ProductWrapper]?
}
