//
//  UserActions.swift
//  Berluti
//
//  Created by Jeremy Martiano on 15/11/2017.
//  Copyright © 2017 elie buff. All rights reserved.
//

import ReSwift


struct UserInfoSetAction: Action {
    let userInfo: [UserDefaultKey:Any]?
}

struct UserInforUpdateValueAction: Action {
    let key: UserDefaultKey
    let value: Any
}

struct IsLoadingUserAction: Action {
    let isLoading: Bool?
}
