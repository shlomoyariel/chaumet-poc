//
//  AppReducer.swift
//  Berluti
//
//  Created by elie buff on 31/10/2017.
//  Copyright © 2017 elie buff. All rights reserved.
//

import ReSwift


    
    func appReducer(action: Action, state: State?) -> State {
        return State(
            userState: UserReducer.getReducer(state: state?.userState, action: action)!,
            configState: ConfigReducer.getReducer(state: state?.configState, action: action)!,
            productState: ProductReducer.getReducer(state: state?.productState, action: action)!,
            emailState: EmailReducer.getReducer(state: state?.emailState, action: action)!
        )
    }
    

