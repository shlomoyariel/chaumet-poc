//
//  PicklistReducer.swift
//  Berluti
//
//  Created by Jeremy Martiano on 15/11/2017.
//  Copyright © 2017 elie buff. All rights reserved.
//

import Foundation
import ReSwift

public class ConfigReducer{
    static func getReducer(state: ConfigState?, action: Action) -> ConfigState? {
        var state = state ?? ConfigState()
        
        switch action {
        case let action as ConfigSetItemsAction:
            state.items = action.items
        case let action as ConfigSelectItemsAction:
            state.selectedItems = action.selectedItems
        case let action as ConfigSetCurrentObject:
            state.currentObject = action.currentObject
        case let action as IsLoadingConfigAction:
            state.isLoading = action.isLoading
        default:
            break
        }
        
        return state
    }
}
