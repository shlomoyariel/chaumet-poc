//
//  EmailReducer.swift
//  IOS Template
//
//  Created by Shlomo Ariel on 31/05/2018.
//  Copyright © 2018 Balink. All rights reserved.
//

import ReSwift

public class EmailReducer{
    static func getReducer(state: EmailState?, action: Action) -> EmailState? {
        var state = state ?? EmailState()
        
        switch action {
        case let action as EmailsIsLoadingAction:
            state.isLoading = action.isLoading
        case let action as SetEmailListAction:
            state.emailItems = action.emailItems
        case let action as SetSelectedEmailAction:
            state.selectedEmail = action.selectedEmail
        default:
            break
        }
        
        return state
    }
}

