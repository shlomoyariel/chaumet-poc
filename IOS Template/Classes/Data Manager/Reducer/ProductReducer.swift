//
//  ProductReducer.swift
//  IOS Template
//
//  Created by Shlomo Ariel on 31/05/2018.
//  Copyright © 2018 Balink. All rights reserved.
//

import ReSwift

public class ProductReducer{
    static func getReducer(state: ProductState?, action: Action) -> ProductState? {
        var state = state ?? ProductState()
    
        switch action {
        case let action as ProductsIsLoadingAction:
            state.isLoading = action.isLoading
        case let action as SetProductListAction:
            state.productItems = action.productItems
        case let action as SetSelectedProductsAction:
            state.selectedProducts = action.selectedProducts
        default:
            break
        }
        
        return state
    }
}

