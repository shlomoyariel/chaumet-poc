//
//  UserReducer.swift
//  Berluti
//
//  Created by Jeremy Martiano on 15/11/2017.
//  Copyright © 2017 elie buff. All rights reserved.
//

import ReSwift

public class UserReducer{
    static func getReducer(state: UserState?, action: Action) -> UserState? {
        var state = state ?? UserState()
        
        switch action {
        case let action as UserInfoSetAction:
            state.userInfo = action.userInfo
        case let action as UserInforUpdateValueAction:
            state.userInfo?[action.key] = action.value
        case let action as IsLoadingUserAction:
            state.isLoading = action.isLoading
        default:
            break
        }
        
        return state
    }
}

