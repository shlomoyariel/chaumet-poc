//
//  PicklistState.swift
//  Berluti
//
//  Created by Jeremy Martiano on 15/11/2017.
//  Copyright © 2017 elie buff. All rights reserved.
//

import Foundation
import UIKit

struct ConfigState {
    var items: [(label :String, value :String)]?
    var selectedItems: [(label :String, value :String)]?
    var currentObject:(object: String,field: String,label: UILabel,isLookup:Bool)?
    var isLoading: Bool?
}

