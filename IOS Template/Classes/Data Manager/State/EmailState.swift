//
//  ProductState.swift
//  IOS Template
//
//  Created by Shlomo Ariel on 31/05/2018.
//  Copyright © 2018 Balink. All rights reserved.
//

import Foundation

struct EmailState {
    var isLoading: Bool?
    var emailItems: [EmailWrapper]?
    var selectedEmail: EmailWrapper?
}
