//
//  UserState.swift
//  Berluti
//
//  Created by Jeremy Martiano on 15/11/2017.
//  Copyright © 2017 elie buff. All rights reserved.
//

struct UserState {
    var userInfo: [UserDefaultKey:Any]?
    var isFiltered: Bool?
    var isLoading: Bool?
}

