//
//  ConfigurationUtils.swift
//  Berluti
//
//  Created by Jeremy Martiano on 15/11/2017.
//  Copyright © 2017 elie buff. All rights reserved.
//

import Foundation
import UIKit
import CoreData
class ConfigurationUtils{
    
    
    static func RetrieveConfigValues(viewController :UIViewController?){
        WebService.ExecuteWS(restMethod: .GET, wsName: "/GetConfigValues", queryParams: nil, body: nil, onFailure: { (err) in
            if let err = err{
                print(err)
                viewController?.checkWSError(error: (title: err.localizedTitle, description: err.localizedDescription))
            }
        }) { (records) in
            if let records = records as? [String : AnyObject]{
                if let picklists = records["Picklists"] as? [[String : AnyObject]] {
                    PickListObject.Upsert(picklists,
                                          in: CoreDataUtils.sharedInstance.createChildContext(),
                                          deleteAllNotIn: true)
                }
                
                if let configData = records["Config_Data"] as? [String : AnyObject] {
                    for (k, v) in configData{
                        if let userDefaultKey = UserDefaultKey(rawValue: k){
                            UserDefaultUtils.saveItem(key: userDefaultKey, value: v)
                        }
                    }
                    DispatchQueue.main.async {
//                        UserUtils.getLocalUserInfo(loadLanguage: false)
                    }
                }
            }
            
        }
    }
    
    
    static func setPicklistValues(for pair:(object:String, field:String), controllingFieldValue:String? = nil)
    {
        var values = PickListObject.getValues(for: pair.field, in: pair.object, with: controllingFieldValue)
        mainStore.dispatch(ConfigSetItemsAction(items: values))
    }
    
    static func SelectPicklist(label :String?, value :String?){
        guard let label = label,  let value = value  else{
            mainStore.dispatch(ConfigSelectItemsAction(selectedItems: nil))
            return
        }
        selectPicklists(picklistValues: [(label: label, value: value)])
    }
    
    static func selectPicklists(picklistValues: [(label: String, value: String)]?){
        guard let picklistValues = picklistValues else{
            mainStore.dispatch(ConfigSelectItemsAction(selectedItems: nil))
            return
        }
        mainStore.dispatch(ConfigSelectItemsAction(selectedItems: picklistValues))
    }
    
    static func getSelectedPicklistValues()-> (labels:String, values:String)?{
        let selectedItems = mainStore.state.configState.selectedItems
        
        return selectedItems?.reduce((labels:"", values:""), { (prev, currentSelectedValue) -> (labels:String, values:String) in
            let separator = prev.labels == "" ? "":";"
            
            return (
                labels: prev.labels + separator + currentSelectedValue.label,
                values: prev.values + separator + currentSelectedValue.value
            )
        })
    }
    
    
    static func getPicklistValue(objectName:String, fieldName:String, label:String, controllingFieldValue:String? = nil)-> String?{
        let picklistData = PickListObject.getValues(for: fieldName, in: objectName, with: controllingFieldValue)
        if let picklistData = picklistData{
            for item in picklistData{
                if item.label.trimmingCharacters(in: .whitespaces) == label.trimmingCharacters(in: .whitespaces){
                    return item.value
                }
            }
        }
        return label
    }
    
    static func getPicklistValues(objectName:String, fieldName:String, labels:String?, controllingFieldValue:String? = nil)-> String{
        var values = ""
        if let labels = labels{
            let labelsArray = labels.components(separatedBy: ";")
            let picklistData = PickListObject.getValues(for: fieldName, in: objectName, with: controllingFieldValue)
            if let picklistData = picklistData{
                values = picklistData.reduce("", { (prev, item) in
                    var result = prev
                    if labelsArray.contains(item.label.trimmingCharacters(in: .whitespaces)){
                        result = (result == "" ? "" : result + ";") + item.value
                    }
                    return result
                })
            }
        }
        return values
    }
    
    
    static func getPicklistLabel(objectName:String, fieldName:String, value:String?)-> String{
        if let value = value{
            let picklistData = PickListObject.getValues(for: fieldName, in: objectName, with: nil)
            if let picklistData = picklistData{
                for item in picklistData{
                    if item.value.trimmingCharacters(in: .whitespaces) == value.trimmingCharacters(in: .whitespaces){
                        return item.label
                    }
                }
            }
        }
        
        return ""
    }
    
    static func getPicklistLabels(objectName:String, fieldName:String, values:String?, controllingFieldValue:String? = nil)-> String{
        var labels = ""
        if let values = values{
            let valuesArray = values.components(separatedBy: ";")
            let picklistData = PickListObject.getValues(for: fieldName, in: objectName, with: controllingFieldValue)
            if let picklistData = picklistData{
                labels = picklistData.reduce("", { (prev, item) in
                    var result = prev
                    if valuesArray.contains(item.value.trimmingCharacters(in: .whitespaces)){
                        result = (result == "" ? "" : result + ";") + item.label
                    }
                    return result
                })
            }
        }
        
        return labels
    }
    
    static func getSelectedPicklistValuesLabels(objectName: String,fieldName:String, selectedValues:String?, controllingFieldValue:String? = nil) -> [(label: String, value: String)]?{
        if let selectedValues = selectedValues{
            let values = selectedValues.components(separatedBy:";")
            var results = [(label: String, value: String)]()
            let picklistData = PickListObject.getValues(for: fieldName, in: objectName, with: controllingFieldValue)
            if let picklistData = picklistData{
                for item in picklistData{
                    if values.contains(item.value.trimmingCharacters(in: .whitespaces)){
                        results.append((label: item.label, value: item.value))
                    }
                }
            }
            
            return results
        }
        return nil
    }
    
    static func setCurrentPicklist(currentPicklist: (object: String, field: String, label: UILabel,isLookup:Bool)?, selectedPair: (labels: String, values: String)?){
        mainStore.dispatch(ConfigSetCurrentObject(currentObject: currentPicklist))
        if let currentPicklist = currentPicklist, currentPicklist.isLookup{
            let pairsArray = selectedPair != nil ? convertToArray(pair:selectedPair!) : nil
            mainStore.dispatch(ConfigSetItemsAction(items: pairsArray))
            selectPicklists(picklistValues: pairsArray)
        } else{
            setPicklistValues(for: ((currentPicklist?.object)!,(currentPicklist?.field)!))
            selectPicklists(picklistValues: getSelectedPicklistValuesLabels(objectName: (currentPicklist?.object)!, fieldName: (currentPicklist?.field)!, selectedValues:selectedPair?.values))
        }
    }
    
    static func convertToArray(pair: (labels: String, values: String))->[(label: String, value: String)]{
        let valuesArray = pair.values.components(separatedBy: ";")
        let labelsArray = pair.labels.components(separatedBy: ";")
        
        return valuesArray.enumerated().map{(index, value) in
            return (label: labelsArray[index], value: value)
        }
    }
    
//    static func getLookupOptions(by searchText:String, objectName:String, viewController: UIViewController?, completionHandler: (() -> ())? = nil)
//    {
//        mainStore.dispatch(IsLoadingConfigAction(isLoading: true))
//        let query = SOQLQueries.getLookupOptionsQuery(searchText: searchText,objectName: objectName)
//        WSGeneral.ExecuteSOQlQuery(query: query, onFailure: { (err) in
//            if let err = err{
//                print(err)
//            }
//        }, onSuccess: { (records) in
//            if let records = records as? [[String : AnyObject]]{
//                var picklistValues = [(label:String, value: String)]()
//                for record in records{
//                    if let Id = record["Id"] as? String, let Label = record["Name"] as? String{
//                        if !picklistValues.contains(where: {$0.label == Label}){
//                            picklistValues.append((label: Label, value: Id))
//                        }
//                    }
//                }
//                DispatchQueue.main.async {
//                    mainStore.dispatch(IsLoadingConfigAction(isLoading: false))
//                    mainStore.dispatch(ConfigSetItemsAction(items: picklistValues))
//                }
//            }
//        })
//    }
    
    static func setCurrentFieldData (currentData: (object: String,field: String,label: UILabel,isLookup:Bool),object: AnyObject?){
        var pair: (labels: String, values: String)? = nil
        if !currentData.isLookup{
            if let labels = currentData.label.text{
                let values = ConfigurationUtils.getPicklistValues(objectName: currentData.object, fieldName: currentData.field, labels: currentData.label.text)
                pair = (labels : labels, values : values)
            }
        } else if let nsObject = object as? NSManagedObject, let values = (nsObject.value(forKey: currentData.field+"Ids") as? String), let labels = (nsObject.value(forKey: currentData.field+"Labels") as? String),values != ""{
            pair = (labels : labels, values : values)
        } else if let objectWrapper = object as? GeneralWrapper, let values = (objectWrapper.getDataFor(key: currentData.field+"Ids") as? String), let labels = (objectWrapper.getDataFor(key: currentData.field+"Labels") as? String),values != ""{
            pair = (labels : labels, values : values)
        }
        
        ConfigurationUtils.setCurrentPicklist(currentPicklist: currentData, selectedPair: pair)
    }
    
//    static func getLookupLabels(by ids:String, objectName:String) -> String
//    {
//        mainStore.dispatch(IsLoadingConfigAction(isLoading: true))
//        let query = SOQLQueries.getLookupLabelsQuery(ids: ids,objectName: objectName)
//        WSClient.ExecuteSOQlQuery(query: query, onFailure: { (err) in
//            if let err = err{
//                print(err)
//            }
//        }, onSuccess: { (records) in
//            if let records = records as? [[String : AnyObject]]{
//                var labels = ""
//                for record in records{
//                    if let Id = record["Id"] as? String, let Label = record["Name"] as? String{
//                        labels = (labels == "" ? "" : labels + ";") + Label
//                    }
//                }
//                return labels
//            }
//        })
//    }
}
