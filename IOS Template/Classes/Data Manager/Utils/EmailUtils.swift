//
//  ProductUtils.swift
//  IOS Template
//
//  Created by Shlomo Ariel on 31/05/2018.
//  Copyright © 2018 Balink. All rights reserved.
//

import Foundation
//
//  StoryUtils.swift
//  Berluti
//
//  Created by elie buff on 12/11/2017.
//  Copyright © 2017 elie buff. All rights reserved.
//
import ReSwift


class EmailUtils{
    static func getEmails(viewController: UIViewController?, onSuccess: (() -> ())? = nil)
    {
        let query = SOQLQueries.getItems(objectName: "Emailtemplate", fields: ["Id","Name","","Subject","Body","HtmlValue"])
        WebService.SOQLExecuter(query: query, onFailure: { (err) in
            if let err = err{
                viewController?.checkWSError(error: (title: err.localizedTitle, description: err.localizedDescription))
            }
        }) { (records) in
            DispatchQueue.main.async {
                var emailList:[EmailWrapper]? = nil
                emailList = GeneralWrapper.fromSF(items: records)
                setEmails(emailList)
                if let onSuccess = onSuccess{
                    onSuccess()
                }
            }
        }
    }
    
    static func getEmail(viewController: UIViewController?, onSuccess: (() -> ())? = nil)
    {
        let query = SOQLQueries.getEmailTemplate()
        WebService.SOQLExecuter(query: query, onFailure: { (err) in
            if let err = err{
                viewController?.checkWSError(error: (title: err.localizedTitle, description: err.localizedDescription))
            }
        }) { (records) in
            DispatchQueue.main.async {
                var emailList:[EmailWrapper]? = nil
                emailList = GeneralWrapper.fromSF(items: records)
                setEmails(emailList)
                if let emailList = emailList, emailList.count > 0{
                    setSelectedEmail(emailList[0])
                }
                if let onSuccess = onSuccess{
                    onSuccess()
                }
            }
        }
    }
    
    static func getProductsIds(for products: [ProductWrapper]) ->String{
        return products.reduce(""){ (prev, productItem) -> String in
            let prefix = prev == "" ? "" : ";"
            return "\(prev)\(prefix)\(productItem.Id!)"
        }
    }
    
    static func setEmails(_ emails: [EmailWrapper]?)
    {
        mainStore.dispatch(
            SetEmailListAction(emailItems: emails)
        )
    }
    
    static func setSelectedEmail(_ email: EmailWrapper?)
    {
        mainStore.dispatch(
            SetSelectedEmailAction(selectedEmail: email)
        )
    }
    
    
}
