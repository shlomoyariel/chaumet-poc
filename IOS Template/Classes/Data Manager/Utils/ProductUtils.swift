//
//  ProductUtils.swift
//  IOS Template
//
//  Created by Shlomo Ariel on 31/05/2018.
//  Copyright © 2018 Balink. All rights reserved.
//

import Foundation
//
//  StoryUtils.swift
//  Berluti
//
//  Created by elie buff on 12/11/2017.
//  Copyright © 2017 elie buff. All rights reserved.
//
import ReSwift


class ProductUtils{
    static func getProducts(viewController: UIViewController?, onSuccess: (() -> ())? = nil)
    {
        let query = SOQLQueries.getItems(objectName: "Product2", fields: ["Id","Name","Material__c","More_Details__c","DisplayUrl","Description"])
        WebService.SOQLExecuter(query: query, onFailure: { (err) in
            if let err = err{
                viewController?.checkWSError(error: (title: err.localizedTitle, description: err.localizedDescription))
            }
        }) { (records) in
            DispatchQueue.main.async {
                var productList:[ProductWrapper]? = nil
                productList = GeneralWrapper.fromSF(items: records)
                setProducts(productList)
                if let onSuccess = onSuccess{
                    onSuccess()
                }
            }
        }
    }
    
    static func getProductsIds(for products: [ProductWrapper]) ->String{
        return products.reduce(""){ (prev, productItem) -> String in
            let prefix = prev == "" ? "" : ";"
            return "\(prev)\(prefix)\(productItem.Id!)"
        }
    }
    
    static func setProducts(_ products: [ProductWrapper]?)
    {
        mainStore.dispatch(
            SetProductListAction(productItems: products)
        )
    }
    
    static func setSelectedProducts(_ products: [ProductWrapper]?)
    {
        mainStore.dispatch(
            SetSelectedProductsAction(selectedProducts: products)
        )
    }
    
    
}
