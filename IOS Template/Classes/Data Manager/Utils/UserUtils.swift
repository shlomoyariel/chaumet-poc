//
//  UserUtils.swift
//  Berluti
//
//  Created by Jeremy Martiano on 15/11/2017.
//  Copyright © 2017 elie buff. All rights reserved.
//

import ReSwift
import SalesforceSDKCore

class UserUtils{
    
    
    static let userDescriptionFields = [UserDefaultKey.firstName,
                                        UserDefaultKey.lastName,
                                        UserDefaultKey.fullPhotoUrl,
                                        UserDefaultKey.userLanguage,
                                        UserDefaultKey.email,
                                        UserDefaultKey.name,
                                        UserDefaultKey.phone,
                                        UserDefaultKey.currency]

    
    
    static func getUserInfoFromServer( onSuccess : (()->Void)?, viewController: UIViewController?){
        let query = SOQLQueries.getUserDetails()
        WebService.SOQLExecuter(query: query, onFailure: { (err) in
            if let err = err{
                viewController?.checkWSError(error: (title: err.localizedTitle, description: err.localizedDescription))
            }
        }) { (result) in
            DispatchQueue.main.async {
                UserDefaultUtils.addItems(keys: self.userDescriptionFields, values: result[0])
                self.getLocalUserInfo(loadLanguage: true)

                if let onSuccess = onSuccess{
                    onSuccess()
                }

            }
        }
    }
    
    static func getLocalUserInfo(loadLanguage: Bool)
    {
        let userInfo = UserDefaultUtils.getItems(keys: UserDefaultKey.allValues)
        mainStore.dispatch(UserInfoSetAction(userInfo: userInfo))
        
        if loadLanguage{
            LocalizeUtils.setupLocalizedBundle(salesForceLang: userInfo[.userLanguage] as? String ?? "en_US")
        }
    }

    static func removeUserInfo()
    {
        UserDefaultUtils.removeItems(keys: [UserDefaultKey.firstName,
                                            UserDefaultKey.lastName,
                                            UserDefaultKey.fullPhotoUrl,
                                            UserDefaultKey.userLanguage])
        mainStore.dispatch(UserInfoSetAction(userInfo: nil ))
    }
    
    static func updateUser(viewController: UIViewController?, profilInfos: [UserDefaultKey: Any],completionHandler: (() -> ())? = nil){
        
        func getAndDispatch(){
            for (key,value) in profilInfos{
                UserDefaultUtils.saveItem(key: key, value: value)
            }
            getLocalUserInfo(loadLanguage: false)
            GeneralUtils.ClearCache(clearCacheType:ClearCacheType.NewStore)
        }
        
        var bodyParams = [String:AnyObject]()
        for (key,value) in profilInfos{
            bodyParams[key.rawValue] = value as AnyObject
        }
        
        mainStore.dispatch(IsLoadingUserAction(isLoading: true))
        WebService.ExecuteWS(restMethod: .POST, wsName: "/User/", queryParams: nil, body:bodyParams, onFailure: { (err) in
            if let err = err{
                print(err)
                mainStore.dispatch(IsLoadingUserAction(isLoading: false))
            }
        }, onComplete: { (records) in
            DispatchQueue.main.async {
                mainStore.dispatch(IsLoadingUserAction(isLoading: false))
                getAndDispatch()
                if completionHandler != nil {
                    completionHandler!()
                }
            }
        })
    }
}

