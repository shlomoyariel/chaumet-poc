//
//  Enums.swift
//  Berluti
//
//  Created by elie buff on 09/11/2017.
//  Copyright © 2017 elie buff. All rights reserved.
//


enum UserDefaultKey : String{
    case lastUpdate = "LastUpdate"
    case lastLogin = "LastLogin"
    case lastClientUpdate = "LastClientUpdate"
    case fullPhotoUrl = "FullPhotoUrl"
    case userLanguage = "LanguageLocaleKey"
    case currency = "DefaultCurrencyIsoCode"
    case firstName = "FirstName"
    case lastName = "LastName"
    case secondName = "SecondName"
    case currentAppVersion = "currentAppVersion"
    case email = "Email"
    case name = "Name"
    case phone = "Phone"


    static let allValues = [lastUpdate,lastLogin,fullPhotoUrl,userLanguage,currency,firstName,lastName,email, phone]
}

enum ClearCacheType{
    case Logout
    case Partial
    case NewVersion
    case Complete
    case NewStore
}

