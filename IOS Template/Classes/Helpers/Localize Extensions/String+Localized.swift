//
//  String.swift
//  C. Everywhere
//
//  Created by Yossi Sud on 19/02/2017.
//  Copyright © 2017 Elie Buff. All rights reserved.
//

import Foundation

extension String {

    func localized(withComment comment:String) -> String {
        return NSLocalizedString(self, tableName: nil, bundle: LocalizeUtils.localizedBundle, value: "", comment: comment)
    }
    
    func localized()-> String {
        return NSLocalizedString(self, tableName: nil, bundle: LocalizeUtils.localizedBundle, value: "", comment: "")
    }
}
