//
//  UIBarItem+Localized.swift
//  ICON
//
//  Created by Yoni on 24/05/2017.
//  Copyright © 2017 elie buff. All rights reserved.
//

import UIKit

extension UIBarItem{
    open override func awakeFromNib() {
        super.awakeFromNib()
        if let _title = self.title, _title != "" {
            self.title = _title.localized()
        }
    }
}
