//
//  UILabel+LocalizedText.swift
//  C. Everywhere
//
//  Created by Yoni on 04/04/2017.
//  Copyright © 2017 Elie Buff. All rights reserved.
//

import UIKit

extension UILabel{
    open override func awakeFromNib() {
        super.awakeFromNib()
        if let _text = self.text, _text != ""{
            self.text = _text.localized()
        }
        
        if let updatedFont = LocalizeHelper.setSpecialFont(currentFont: font.familyName, currentSize: font.pointSize){
            self.font = updatedFont
        }
    }
}
