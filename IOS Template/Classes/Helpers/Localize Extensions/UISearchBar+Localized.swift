//
//  UISearchBar+Localized.swift
//  ICON
//
//  Created by Yoni on 24/05/2017.
//  Copyright © 2017 elie buff. All rights reserved.
//

import UIKit

extension UISearchBar{
    open override func awakeFromNib() {
        super.awakeFromNib()
        if let _placeholder = self.placeholder, _placeholder != "" {
            self.placeholder = _placeholder.localized()
        }
        if let _prompt = self.prompt, _prompt != "" {
            self.prompt = _prompt.localized()
        }
    }
}
