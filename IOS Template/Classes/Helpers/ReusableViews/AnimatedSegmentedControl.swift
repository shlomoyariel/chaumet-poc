//
//  AnimatedSegmentedControl.swift
//  Berluti
//
//  Created by elie buff on 29/11/2017.
//  Copyright © 2017 elie buff. All rights reserved.
//

import UIKit

@IBDesignable
class AnimatedSegmentedControl: UIView {

    @IBOutlet weak var parentView: UIView!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var buttonBar: UIView!
    @IBOutlet weak var buttonBarWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var buttonBarLeadingConstraint: NSLayoutConstraint!
    
    var contentView : UIView?
    var segmentedValueChanged : ((Int) ->())?
    
    @IBInspectable var segments : [String]?{
        didSet{
            if let segments = segments{
                segmentedControl.removeAllSegments()
                for (index,segment) in segments.enumerated(){
                    segmentedControl.insertSegment(withTitle: segment.localized(), at: index, animated: false)
                }
                segmentedControl.selectedSegmentIndex = 0
                if let segmentedValueChanged = segmentedValueChanged{
                    segmentedValueChanged(self.segmentedControl.selectedSegmentIndex)
                }
                updateSegmentedControlDisplay()
            }
        }
    }
    
    override func awakeFromNib() {
        segmentedControl.setTitleTextAttributes([
            NSAttributedStringKey.font : UIFont.systemFont(ofSize: 15, weight: .bold),
            NSAttributedStringKey.foregroundColor: #colorLiteral(red: 0.1450980392, green: 0.1450980392, blue: 0.1450980392, alpha: 1)
            ], for: .normal)
        segmentedControl.setTitleTextAttributes([
            NSAttributedStringKey.font : UIFont.systemFont(ofSize: 15, weight: .bold),
            NSAttributedStringKey.foregroundColor: #colorLiteral(red: 0.01176470588, green: 0.6392156863, blue: 1, alpha: 1)
            ], for: .selected)
    }
    
    func updateSegmentedControlDisplay(){
        let newConstraint = self.buttonBarWidthConstraint.constraintWithMultiplier( 1 / CGFloat(segmentedControl.numberOfSegments))
        self.parentView!.removeConstraint(self.buttonBarWidthConstraint)
        buttonBarWidthConstraint = newConstraint
        self.parentView!.addConstraint(buttonBarWidthConstraint)
        contentView?.layoutIfNeeded()
    }
    
    @IBAction func segmentedValueChanged(_ sender: Any) {
        UIView.animate(withDuration: 0.3) {
            self.buttonBarLeadingConstraint.constant = (self.segmentedControl.frame.width / CGFloat(self.segmentedControl.numberOfSegments)) * CGFloat(self.segmentedControl.selectedSegmentIndex)
            self.contentView?.layoutIfNeeded()
        }
        
        if let segmentedValueChanged = segmentedValueChanged{
            segmentedValueChanged(self.segmentedControl.selectedSegmentIndex)
        }
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    
    func xibSetup() {
        contentView = loadViewFromNib()
        // use bounds not frame or it'll be offset
        contentView!.frame = bounds
        
        // Make the view stretch with containing view
        contentView!.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
        
        // Adding custom subview on top of our view (over any custom drawing > see note below)
        addSubview(contentView!)
    }
    
    func loadViewFromNib() -> UIView! {
        
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        
        return view
    }
}
