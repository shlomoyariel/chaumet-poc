//
//  UIView + CustomAlert.swift
//  Berluti
//
//  Created by elie buff on 15/01/2018.
//  Copyright © 2018 elie buff. All rights reserved.
//

import UIKit

extension UIViewController {
    
    func customConfirmMessage(messageLabel :UILabel, iconName :String, onConfirm: (() -> ())? = nil){
        
        let _ = CustomPopup(subjectText: "Are you sure?", subtitleLbl: messageLabel, image: UIImage(named: iconName), buttonsArray: [
            (title:"CONFIRM",backgroundColor: #colorLiteral(red: 0.3843137255, green: 0.8509803922, blue: 0.3882352941, alpha: 1))
        ]) { (tag) in
            if tag == 0{
                if let onConfirm = onConfirm{
                    onConfirm()
                }
            }
        }
    }
    
    func customDoneMessage(messageLabel :UILabel, iconName :String, onConfirm: (() -> ())? = nil){
        
        let _ = CustomPopup(subjectText: "Done", subtitleLbl: messageLabel, image: UIImage(named: iconName), buttonsArray: [
            (title:"OK",backgroundColor: #colorLiteral(red: 0.3843137255, green: 0.8509803922, blue: 0.3882352941, alpha: 1))
        ]) { (tag) in
            if tag == 0{
                if let onConfirm = onConfirm{
                    onConfirm()
                }
            }
        }
    }
    
    func customCancelConfirmMessage(messageLabel :UILabel, iconName :String, onConfirm: (() -> ())? = nil){
        let _ = CustomPopup(subjectText: "Are you sure?", subtitleLbl: messageLabel, image: UIImage(named: iconName), buttonsArray: [
            (title:"CANCEL",backgroundColor: #colorLiteral(red: 0.9764705882, green: 0.2352941176, blue: 0.1607843137, alpha: 1)),
            (title:"VALIDATE",backgroundColor: #colorLiteral(red: 0.3843137255, green: 0.8509803922, blue: 0.3882352941, alpha: 1))
        ]) { (tag) in
            if tag == 1{
                if let onConfirm = onConfirm{
                    onConfirm()
                }
            }
        }
    }
    
    func customErrorMessage(messageLabel :UILabel, iconName :String, onConfirm: (() -> ())? = nil){
        let _ = CustomPopup(subjectText: "Something went wrong", subtitleLbl: messageLabel, image: UIImage(named: iconName), buttonsArray: [(title:"OK",backgroundColor: #colorLiteral(red: 0.9764705882, green: 0.2352941176, blue: 0.1607843137, alpha: 1))],onComplete: nil)
    }
}




