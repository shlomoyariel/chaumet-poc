//
//  PickListObject+CoreDataClass.swift
//  IOS Template
//
//  Created by Jeremy Martiano on 22/04/2018.
//  Copyright © 2018 Balink. All rights reserved.
//
//

import Foundation
import CoreData


import Foundation
import CoreData


public class PickListObject: NSManagedObject, NSManagedObjectParent {
    static var SFObjectName = ""
    static var entityName: String = "PickListObject"
    
    static var salesforceMapping = ["id" : (salesforceName:"Id", mapToServer:true),
                                    "fieldName": (salesforceName:"FieldName", mapToServer:true),
                                    "objectName":(salesforceName:"ObjectName", mapToServer:true),
                                    "controllingFieldValue":(salesforceName:"ControllingFieldValue", mapToServer:true),
                                    "values":(salesforceName:"Values", mapToServer:true),
                                    "labels":(salesforceName:"Labels", mapToServer:true)]
    
    static func getValues(for field:String, in object:String, with controllingFieldValue: String?) -> [(label :String, value :String)]?
    {
        let context = CoreDataUtils.sharedInstance.context
        let valuesFetch = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
        
        var subpredicates = [NSPredicate]();
        subpredicates.append(NSPredicate(format:"objectName = %@", object))
        subpredicates.append(NSPredicate(format:"fieldName = %@", field))
        
        if let controllingFieldValue = controllingFieldValue{
            subpredicates.append(NSPredicate(format:"controllingFieldValue = %@", controllingFieldValue))
        }
        
        let andPredicate = NSCompoundPredicate(type: NSCompoundPredicate.LogicalType.and, subpredicates: subpredicates)
        
        valuesFetch.predicate = andPredicate
        
        do {
            let fetchedPicklists = try context?.fetch(valuesFetch) as! [PickListObject]?
            if let picklistValues = fetchedPicklists, picklistValues.count > 0{
                var result = [(label :String, value :String)]()
                
                let values = picklistValues[0].values?.components(separatedBy: ";")
                let labels = picklistValues[0].labels?.components(separatedBy: ";")
                
                if let values = values, let labels = labels{
                    for (index, _) in values.enumerated(){
                        result.append((label :labels[index], value :values[index]))
                    }
                }
                
                return result
            }
        } catch {
            fatalError("Failed to fetch tasks: \(error)")
        }
        return nil
    }
}

