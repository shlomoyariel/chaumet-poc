//
//  PickListObject+CoreDataProperties.swift
//  IOS Template
//
//  Created by Jeremy Martiano on 22/04/2018.
//  Copyright © 2018 Balink. All rights reserved.
//
//

import Foundation
import CoreData


extension PickListObject {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<PickListObject> {
        return NSFetchRequest<PickListObject>(entityName: "PickListObject")
    }

    @NSManaged public var controllingFieldValue: String?
    @NSManaged public var fieldName: String?
    @NSManaged public var labels: String?
    @NSManaged public var id: String?
    @NSManaged public var objectName: String?
    @NSManaged public var values: String?

}
