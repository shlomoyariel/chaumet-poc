//
//  EmailWrapper.swift
//  IOS Template
//
//  Created by Shlomo Ariel on 03/06/2018.
//  Copyright © 2018 Balink. All rights reserved.
//

import CoreData
import SalesforceSDKCore

class EmailWrapper: GeneralWrapper{
    var Name : String?{
        get{ return self.getDataFor(key: "Name") as? String}
    }
    var Subject : String?{
        get{ return self.getDataFor(key: "Subject") as? String}
    }
    
    var HtmlValue : String?{
        get{ return self.getDataFor(key: "HtmlValue") as? String}
    }
    
    var Body : String?{
        get{ return self.getDataFor(key: "Body") as? String}
    }
    
    
}
