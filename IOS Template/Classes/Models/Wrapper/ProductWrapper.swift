//
//  Order.swift
//  Berluti
//
//  Created by Shlomo Ariel on 31/12/2017.
//  Copyright © 2017 elie buff. All rights reserved.
//

import CoreData
import SalesforceSDKCore

class ProductWrapper: GeneralWrapper{
    var Name : String?{
        get{ return self.getDataFor(key: "Name") as? String}
    }
    
    var UnitPrice : Int?{
        get{ return self.getDataFor(key: "UnitPrice") as? Int}
    }
    
    var Material__c : String?{
        get{ return self.getDataFor(key: "Material__c") as? String}
    }
    
    var More_Details__c : String?{
        get{ return self.getDataFor(key: "More_Details__c") as? String}
    }
    
    var Description : String?{
        get{ return self.getDataFor(key: "Description") as? String}
    }
    
    var Quantity : Int?{
        get{ return self.getDataFor(key: "Quantity") as? Int}
    }
    
    var DisplayUrl : String?{
        get{ return self.getDataFor(key: "DisplayUrl") as? String}
    }
    
    var Image__c : String?{
        get{ return self.getDataFor(key: "Image__c") as? String}
    }
    
    var Family : String?{
        get{ return self.getDataFor(key: "Family") as? String}
    }
    
    var Price__c : Int?{
        get{ return self.getDataFor(key: "Price__c") as? Int}
    }
    
    var PurchaseDate : Date?{
        get{
            var finalDate:Date? = nil
            if let date = self.getDataFor(key: "PurchaseDate") as? String{
                finalDate = Date.stringToDate(stringDate: date)
            }
            return finalDate
        }
    }
    
}


