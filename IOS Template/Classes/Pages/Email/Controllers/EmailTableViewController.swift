//
//  EmailTableViewController.swift
//  IOS Template
//
//  Created by Shlomo Ariel on 03/06/2018.
//  Copyright © 2018 Balink. All rights reserved.
//

import UIKit
import ReSwift
import RichEditorView
import MessageUI

class EmailTableViewController: UITableViewController, RichEditorDelegate {
    
    @IBOutlet weak var templateLabel: UILabel!
    @IBOutlet weak var editorView: RichEditorView!
    @IBOutlet weak var subjectText: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView()
        editorView.delegate = self
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        
        return  super.tableView(tableView,heightForRowAt:indexPath)
    }
    @IBAction func doneTapped(_ sender: Any) {
        dismiss(animated: true)
    }
}

extension EmailTableViewController: StoreSubscriber{
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        mainStore.subscribe(self)
        self.setTabBarVisible(visible:false, animated: true)
        self.tabBarController?.tabBar.fadeOut()
        self.navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.9647058824, green: 0.9647058824, blue: 0.9647058824, alpha: 1)
        self.navigationController?.navigationBar.tintColor = #colorLiteral(red: 0.01176470588, green: 0.6392156863, blue: 1, alpha: 1)
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        mainStore.unsubscribe(self)
    }
    
    func newState(state: State) {
        if let selectedEmail = state.emailState.selectedEmail{
            self.subjectText.text = selectedEmail.Subject
            var messageBody = ""
            messageBody = selectedEmail.HtmlValue!
            let productList = self.arrangeProductList(state.productState.selectedProducts)
            messageBody = messageBody.replacingOccurrences(of: "{!productDetails}",with: productList)
            self.editorView.html = messageBody
        }
    }
    
    func arrangeProductList(_ products:[ProductWrapper]?) -> String{
        var productList = "<br><br>"
        if let selectedProducts = products{
            for product in selectedProducts{
                if let name = product.Name{
                    productList = "\(productList)<b>Name:</b> \(name) <br>"
                }
                if let Material__c = product.Material__c{
                    productList = "\(productList)<b>Material:</b> \(Material__c)<br>"
                }
                if let Description = product.Description{
                    productList = "\(productList)<b>Description:</b> \(Description)<br>"
                }
                if let imageUrl = product.DisplayUrl{
                    productList = "\(productList)<div style=\"display: flex; flex-direction: row; flex-wrap: nowrap; justify-content: center; align-items: center; align-content: stretch;\"><img style=\"height:150px;\" src='\(imageUrl)'/> </div>"
                }
            }
        }
        return productList
    }
}

extension EmailTableViewController : MFMessageComposeViewControllerDelegate, MFMailComposeViewControllerDelegate{
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        controller.dismiss(animated: false, completion: nil)
        
        if result == .sent{
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: false, completion: nil)
        
        if result == .sent{
            self.dismiss(animated: true, completion: nil)
        }
        
    }
    
    @IBAction func done(_ sender: Any) {
        func sendEmail(){
            let messageComposer = MessageComposer()
            if messageComposer.sendMail(to : [], bccEmails: [],
                                        subject: subjectText.text,
                                        body: editorView.html,
                                        isBodyHtml: true,
                                        viewController: self,
                                        mailViewController:  self as MFMailComposeViewControllerDelegate){}
        }
        
        sendEmail()
        /*
        func onComplete(tag:Int){
            switch tag{
            case 0:
                sendEmail()
            default:
                break
            }
        }
        if let clientsCount = self.clients?.count, clientsCount > 1{
            //remove keyboard on popup
            view.endEditing(true)
            
            let subtitleLbl = UILabel()
            subtitleLbl.text = String(format: "send_email_to_clients".localized(), clientsCount)
            subtitleLbl.setBold(word: "\(clientsCount) clients")
            subtitleLbl.setColor(word: "\(clientsCount) clients", color: #colorLiteral(red: 0, green: 0.7057664394, blue: 1, alpha: 1))
            
            CustomPopup(subjectText: "Sending Confirmation".localized(), subtitleLbl: subtitleLbl, image: UIImage(named:"sendEmails"),buttonsArray: [
                (title:"SEND EMAILS",backgroundColor: #colorLiteral(red: 0, green: 0.7057664394, blue: 1, alpha: 1))
                ], onComplete:onComplete)
        } else {
            sendEmail()
        }
        */
    }
    
}






