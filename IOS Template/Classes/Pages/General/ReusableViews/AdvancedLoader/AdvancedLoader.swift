//
//  AdvancedLoader.swift
//  Berluti
//
//  Created by elie buff on 05/02/2018.
//  Copyright © 2018 elie buff. All rights reserved.
//

import UIKit
import FLAnimatedImage

class AdvancedLoader: UIView {
    var contentView : UIView?
    
    @IBOutlet weak var splashLogo: UIImageView!
    @IBOutlet weak var loader: FLAnimatedImageView!
    @IBOutlet weak var logoWidth: NSLayoutConstraint!
    @IBOutlet weak var loaderWidth: NSLayoutConstraint!
    @IBOutlet weak var loaderTitle: UILabel!
    @IBOutlet weak var loaderSubtitle: UILabel!
    
    var title: String? {
        didSet {
            if let title = title{
                loaderTitle.text = title
            }
        }
    }
    
    var subtitle: String? {
        didSet {
            if let subtitle = subtitle{
                loaderSubtitle.text = subtitle
            }
        }
    }
    
    var isLoading: Bool?{
        didSet{
            if let isLoading = isLoading{
                if isLoading {
                    self.fadeIn(0.05, delay: 0.0)
                } else {
                    self.fadeOut()
                }
            }
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    
    func xibSetup() {
        contentView = loadViewFromNib()
        // use bounds not frame or it'll be offset
        contentView!.frame = bounds
        
        // Make the view stretch with containing view
        contentView!.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
        
        // Adding custom subview on top of our view (over any custom drawing > see note below)
        addSubview(contentView!)
    }
    
    func loadViewFromNib() -> UIView! {
        
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        
        setupAnimation()
        
        return view
    }
    
    func setupAnimation(){
        logoWidth.constant = 200
        loaderWidth.constant = 30
        let FLAimage:FLAnimatedImage = FLAnimatedImage(animatedGIFData: Utils.getData(name: "loader"))
        loader.animatedImage = FLAimage
        UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 0.6, initialSpringVelocity: 0.5, options: .curveEaseInOut, animations: {
            self.splashLogo.layoutIfNeeded()
        }, completion: nil)
        
        UIView.animate(withDuration: 1, delay: 0.2, usingSpringWithDamping: 0.6, initialSpringVelocity: 0.7, options: .curveEaseInOut, animations: {
            self.loader.layoutIfNeeded()
        }, completion: nil)
        
        loaderTitle.transform = CGAffineTransform(scaleX: 0, y: 0)
        UIView.animate(withDuration: 1, delay: 0.4, usingSpringWithDamping: 0.6, initialSpringVelocity: 0.7, options: .curveEaseInOut, animations: {
            self.loaderTitle.transform = CGAffineTransform(scaleX: 1, y: 1)
        }, completion: nil)
        
        loaderSubtitle.transform = CGAffineTransform(scaleX: 0, y: 0)
        UIView.animate(withDuration: 1, delay: 0.6, usingSpringWithDamping: 0.6, initialSpringVelocity: 0.7, options: .curveEaseInOut, animations: {
            self.loaderSubtitle.transform = CGAffineTransform(scaleX: 1, y: 1)
        }, completion: nil)
    }
}

