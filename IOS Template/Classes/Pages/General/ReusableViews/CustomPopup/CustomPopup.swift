//
//  Popup.swift
//  Berluti
//
//  Created by Jeremy Martiano on 18/12/2017.
//  Copyright © 2017 elie buff. All rights reserved.
//

import UIKit

class CustomPopup: UIView {
    var contentView : UIView?
    
    @IBOutlet weak var subtitle: UILabel!
    @IBOutlet weak var mainText: UILabel!
    @IBOutlet weak var icone: UIImageView!
    @IBOutlet weak var buttonsSV: UIStackView!
    

    @IBAction func cancel(_ sender: UIButton) {
        onClose(sender: sender)
    }
    
    var onComplete: ((_ tag: Int)->())?

    @objc func onClose(sender:UIButton){
        if let onComplete = onComplete{
            onComplete(sender.tag)
        }
        UIView.animate(withDuration: 0.3, animations: {
            self.alpha = 0
        }) { (true) in
            self.removeFromSuperview()
        }
    }
    
    init(subjectText:String?,subtitleLbl:UILabel?,image:UIImage?,buttonsArray: [(title:String,backgroundColor: UIColor)]?,onComplete:((Int)->())?){
        guard let app = UIApplication.shared.delegate as? AppDelegate, let window = app.window else{
            super.init(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
            return
        }
        super.init(frame:CGRect(x: window.frame.origin.x, y: window.frame.origin.y, width: window.frame.width, height: window.frame.height))
        xibSetup()
        
        
        if let iconeImg = image{
            self.icone.image = iconeImg
        }
        if let subjectLbl = subjectText{
            self.mainText.text = subjectLbl
        }
        if let subtitleLbl = subtitleLbl{
            self.subtitle.attributedText = subtitleLbl.attributedText
            self.subtitle.textAlignment = .center
            self.subtitle.numberOfLines = 0
        }
        
        self.onComplete = onComplete
        
        if let buttonsArray = buttonsArray{
            for (index,item) in buttonsArray.enumerated(){
                let button = BorderedButton()
                button.setTitle(item.title, for: .normal)
                button.setTitleColor(UIColor.white, for: .normal)
                button.titleLabel?.font = UIFont.systemFont(ofSize: 14, weight: .semibold)
                button.backgroundColor = item.backgroundColor
                button.cornerRadius = 25
                button.translatesAutoresizingMaskIntoConstraints = false
                button.tag = index
                button.addTarget(self, action: #selector(onClose(sender:)), for: .touchUpInside)
                self.buttonsSV.addArrangedSubview(button)
            }
        }
        
        self.alpha = 0
        self.isHidden = false
        window.addSubview(self)
        UIView.animate(withDuration: 0.3, animations: {
            self.alpha = 1
        })
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    
    func xibSetup() {
        contentView = loadViewFromNib()
        // use bounds not frame or it'll be offset
        contentView!.frame = bounds
        
        // Make the view stretch with containing view
        contentView!.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
        
        // Adding custom subview on top of our view (over any custom drawing > see note below)
        addSubview(contentView!)
    }
    
    func loadViewFromNib() -> UIView! {
        
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        
        return view
    }
    
}
