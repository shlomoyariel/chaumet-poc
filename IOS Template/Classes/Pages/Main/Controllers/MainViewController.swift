//
//  MainViewController.swift
//  IOS Template
//
//  Created by Shlomo Ariel on 31/05/2018.
//  Copyright © 2018 Balink. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        ProductUtils.getProducts(viewController: self)
        EmailUtils.getEmail(viewController: self)
        self.tabBarController?.tabBar.isHidden = false
        self.tabBarController?.tabBar.barTintColor = #colorLiteral(red: 0.003921568627, green: 0.0431372549, blue: 0.3058823529, alpha: 1)
    }
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}
