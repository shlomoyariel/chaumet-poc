//
//  ProductCollectionCell.swift
//  IOS Template
//
//  Created by Shlomo Ariel on 31/05/2018.
//  Copyright © 2018 Balink. All rights reserved.
//

import UIKit
import Kingfisher

class ProductCollectionCell: UICollectionViewCell{
    @IBOutlet weak var favImage: UIImageView!
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var productType: UILabel!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var price: UILabel!
    
    var productItem: ProductWrapper! {
        didSet {
            self.name.text = productItem.Name ?? ""
            if let price = productItem.Price__c{
                self.price.text = String(describing: price.toCurrency())
            } else {
                self.price.text = ""
            }
            if let pictureUrl = productItem.DisplayUrl{
                image.kf.setImage(with: URL(string: pictureUrl)!, placeholder: #imageLiteral(resourceName: "bracelet image"), options: nil, progressBlock: nil)
            } else {
                image.image =  #imageLiteral(resourceName: "bracelet image")
            }
        }
    }
    
}

