//
//  ProductListViewController.swift
//  IOS Template
//
//  Created by Shlomo Ariel on 31/05/2018.
//  Copyright © 2018 Balink. All rights reserved.
//

import UIKit
import ReSwift

class ProductListViewController: UIViewController {
    
    @IBOutlet weak var productCollectionView: UICollectionView!
    @IBOutlet weak var emptyListView: EmptyListView!
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var firstTabHighlight: UIView!
    @IBOutlet weak var secondTabHighlight: UIView!
    @IBOutlet weak var firstTabLabel: UILabel!
    @IBOutlet weak var secondTabLabel: UILabel!
    var itemList = [ProductWrapper]()
    var selectedItems = [ProductWrapper]()
    var filterdItemList = [ProductWrapper]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ProductUtils.getProducts(viewController: self)
        EmailUtils.getEmail(viewController: self)
        tabTap(firstLabel: secondTabLabel , secondLabel: firstTabLabel, highlight: firstTabHighlight, unhighlight:secondTabHighlight, family: "Joaillerie")
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillDisappear), name: Notification.Name.UIKeyboardWillHide, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillAppear), name: Notification.Name.UIKeyboardWillShow, object: nil)
        
    }
    @IBAction func firstTabAction(_ sender: Any) {
        tabTap(firstLabel: secondTabLabel , secondLabel: firstTabLabel, highlight: firstTabHighlight, unhighlight:secondTabHighlight, family: "Joaillerie")
    }
    @IBAction func secondTabAction(_ sender: Any) {
        tabTap(firstLabel: firstTabLabel, secondLabel: secondTabLabel, highlight: secondTabHighlight, unhighlight: firstTabHighlight, family: "Horlogerie")
    }
    
    func tabTap(firstLabel:UILabel, secondLabel:UILabel, highlight:UIView, unhighlight:UIView, family:String){
        searchBar.text = ""
        firstTabLabel.textColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        secondTabLabel.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        unhighlight.isHidden = true
        highlight.isHidden = false
        self.filterdItemList = itemList.filter {   $0.Family != nil && $0.Family == family }
        self.productCollectionView.reloadData()
    }
    
    @objc func dismissKeyboardCustom() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    @objc func keyboardWillAppear() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self,action: #selector(self.dismissKeyboardCustom))
        view.addGestureRecognizer(tap)
    }
    
    @objc func keyboardWillDisappear() {
        view.gestureRecognizers?.removeAll()
    }
}

extension ProductListViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return self.filterdItemList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let item = filterdItemList[indexPath.row]
        if selectedItems.contains(item){
            selectedItems = selectedItems.filter() { $0 !== item }
        } else{
            selectedItems.append(item)
        }
        
        ProductUtils.setSelectedProducts(selectedItems)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) ->
        UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "productItemCell", for: indexPath) as! ProductCollectionCell
        cell.productItem = filterdItemList[indexPath.row]
        let item = filterdItemList[indexPath.row]
        if selectedItems.contains(item){
            cell.favImage.image = #imageLiteral(resourceName: "favEnabled")
        } else{
            cell.favImage.image = #imageLiteral(resourceName: "favDisabled")
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        let padding: CGFloat =  1
        let collectionViewSize = collectionView.frame.size.width - padding
        
        return CGSize(width: (collectionViewSize/2), height: (190))
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat
    {
        return 1
    }
}

extension ProductListViewController: StoreSubscriber{
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        self.navigationController?.navigationBar.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
//        self.tabBarController?.tabBar.isHidden = false
        self.setTabBarVisible(visible:true, animated: true)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        self.tabBarController?.tabBar.fadeIn()
        self.tabBarController?.tabBar.barTintColor = #colorLiteral(red: 0.003921568627, green: 0.0431372549, blue: 0.3294117647, alpha: 1)
        self.tabBarController?.tabBar.tintColor = #colorLiteral(red: 0.7529411765, green: 0.5450980392, blue: 0.2901960784, alpha: 1)
        mainStore.subscribe(self){ subcription in
            subcription.select { state in state.productState }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        mainStore.unsubscribe(self)
    }
    
    func newState(state: ProductState) {
        if let products = state.productItems{
            if let stateSelectedProducts = state.selectedProducts{
                    self.selectedItems = stateSelectedProducts
            }
            if self.itemList.count == 0{
                self.itemList = products
                self.filterdItemList = products
            }
            
            self.productCollectionView.reloadData()
        }
    }
}

extension UIViewController {
    
    func setTabBarVisible(visible: Bool, animated: Bool) {
        //* This cannot be called before viewDidLayoutSubviews(), because the frame is not set before this time
        
        // bail if the current state matches the desired state
        if (isTabBarVisible == visible) { return }
        
        // get a frame calculation ready
        let frame = self.tabBarController?.tabBar.frame
        let height = frame?.size.height
        let offsetY = (visible ? -height! : height)
        
        // zero duration means no animation
        let duration: TimeInterval = (animated ? 0.3 : 0.0)
        
        //  animate the tabBar
        if frame != nil {
            UIView.animate(withDuration: duration) {
                self.tabBarController?.tabBar.frame = frame!.offsetBy(dx: 0, dy: offsetY!)
                return
            }
        }
    }
    
    var isTabBarVisible: Bool {
        return (self.tabBarController?.tabBar.frame.origin.y ?? 0) < self.view.frame.maxY
    }
}
extension ProductListViewController:UISearchResultsUpdating, UISearchBarDelegate{
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String)
    {
        filterContentForSearchText()
        self.productCollectionView.reloadData()
    }
    
    func updateSearchResults(for searchController: UISearchController)
    {
        filterContentForSearchText()
        self.productCollectionView.reloadData()
    }
    
    func filterContentForSearchText() {
        let searchText = self.searchBar.text!.lowercased()
        self.filterdItemList = self.itemList.filter { item in
            if searchText == ""{
                return true
            }
            else if let Name = item.Name?.lowercased(), Name.contains(searchText){
                return true
            }
            else if let Family = item.Family?.lowercased(), Family.lowercased().contains(searchText.lowercased()){
                return true
            }
            else if let Material__c = item.Material__c?.lowercased(), Material__c.contains(searchText){
                return true
            }
            return false
        }
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        searchBar.setShowsCancelButton(false, animated: true)
    }
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(true, animated: true)
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        searchBar.setShowsCancelButton(false, animated: true)
    }
}
