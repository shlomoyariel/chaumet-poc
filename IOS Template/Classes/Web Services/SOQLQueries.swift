//
//  SOQLQueries.swift
//  Berluti
//
//  Created by elie buff on 12/11/2017.
//  Copyright © 2017 elie buff. All rights reserved.
//

import Foundation
import CoreData
import SalesforceSDKCore

class SOQLQueries{
    let currentUserId = SFUserAccountManager.sharedInstance().currentUserIdentity!.userId ?? ""
    
    
    static func getUserDetails() -> String
    {
        let currentUserId = SFUserAccountManager.sharedInstance().currentUserIdentity!.userId ?? ""
        
        let userStringValue =  UserUtils.userDescriptionFields.map {
            return $0.rawValue
        }
        
        return "SELECT Id, \(userStringValue.joined(separator: ",")) FROM User WHERE Id = '\(currentUserId)'"
    }
    
    static func getLookupOptionsQuery(searchText:String,objectName:String) -> String{
        return "SELECT Id, Name FROM \(objectName) WHERE Name LIKE '%\(searchText)%'"
    }
    
    static func getLookupLabelsQuery(ids:String,objectName:String) -> String{
        let listIds = Utils.listToSOQLString(list: ids.components(separatedBy: ";"))
        return "SELECT Id, Name FROM \(objectName) WHERE Id IN '%\(listIds)%'"
    }
    
    static func getItems(objectName:String, fields:[String]) -> String{
        return "SELECT Id,Name,Material__c,More_Details__c,DisplayUrl,Description,Family, Price__c, Image__c FROM Product2"// "SELECT \(fields.joined(separator: ",")) FROM \(objectName) '"
    }
    
    static func getEmailTemplate () -> String{
        return "SELECT Id, Body, HtmlValue, Subject from Emailtemplate where id = '00X0N000000iPYr'"
    }
    
    
}
